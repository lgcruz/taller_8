#include <stdio.h>
#include <string.h>
#include <stdlib.h>
char *diccionarioMorsa(int c);
int esMayuscula(char c);

int esMayuscula(char c){
	if (c<=90 && c>=65){
		return 1;
	}
	return 0;
}


char *diccionarioMorsa(int c){
	
	char *morsa="";
	switch(c){
		
			case 65://A
				strcpy(morsa, ".- ");
			break;
			case 66://B
				strcpy(morsa, "-... ");
			break;	
			case 67://C
				strcpy(morsa,"-.-. ");
			break;
			case 68://D
				strcpy(morsa, "-.. ");
			break;
			case 69://E
				strcpy(morsa, ". ");
			break;
			case 70://F
				strcpy(morsa, "..-. ");
			break;
			case 71://G
				strcpy(morsa, "--. ");
			break;
			case 72://H
				strcpy(morsa, ".... ");
			break;	
			case 73://I
				strcpy(morsa, ".. ");
			break;
			case 74://J
				strcpy(morsa, ".--- ");
			break;	
			case 75://K
				strcpy(morsa, "-.- ");
			break;
			case 76://L
				strcpy(morsa, ".-.. ");
			break;
			case 77://M
				strcpy(morsa, "-- ");
			break;
			case 78://N
				strcpy(morsa, "-. ");
			break;
			case 79://O
				strcpy(morsa, "--- ");
			break;
			case 80://P
				strcpy(morsa, ".--. ");
			break;
			case 81://Q
				strcpy(morsa, "--.- ");
			break;
			case 82://R
				strcpy(morsa, ".-. ");
			break;	
			case 83://S
				strcpy(morsa, "... ");
			break;
			case 84://T
				strcpy(morsa, "- ");
			break;	
			case 85://U
				strcpy(morsa, "..- ");
			break;
			case 86://V
				strcpy(morsa, "...- ");
			break;
			case 87://W
				strcpy(morsa, ".-- ");
			break;
			case 88://X
				strcpy(morsa, "-..- ");
			break;
			case 89://Y
				strcpy(morsa, "-.-- ");
			break;
			case 90://Z
				strcpy(morsa, "--.. ");
			break;
		}
	return morsa;
}

char *morse(char *msg){
	int i;
	char *frase="";
	for( i=0; i<strlen(msg); i++){
		if(msg[i]!=32){
			if(esMayuscula(msg[i])==1){
				strcat(frase, diccionarioMorsa(msg[i]));	
			}
			else{
				strcat(frase, diccionarioMorsa(msg[i]-32));
				//printf(" %d", msg[i]-32);
			}
		}
		else{
			strcat(frase, "/ ");
		}	
	}
	
	return frase;
}
void mostrar(char *msg);
void mostrar(char *msg){
	int cont=0;

	while(1){
			if(msg[cont]==0)
		break;
		printf("%c", msg[cont]);
		
		cont++;
		
	}
}

