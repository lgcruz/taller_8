all : estatico dinamico

estatico: main.o libcriptops.a
	gcc -g obj/main.o lib/libcriptops.a -o bin/estatico
dinamico: main.o libcriptops.so
	gcc -L /lib obj/main.o lib/libcriptops.so -o bin/dinamico
main.o: src/main.c
	gcc -g -Wall -c -I include/ src/main.c -o obj/main.o
encriptacion.o: src/encriptacion.c
	gcc -g -Wall -c src/encriptacion.c -o obj/encriptacion.o
morse.o: src/morse.c
	gcc -g -Wall -c src/morse.c -o obj/morse.o
libcriptops.a: encriptacion.o morse.o
	ar rcs lib/libcriptops.a obj/morse.o obj/encriptacion.o
encriptacion_din.o: src/encriptacion.c
	gcc -g -Wall -c -fPIC src/encriptacion.c -o obj/encriptacion_din.o
morse_din.o: src/morse.c
	gcc -g -Wall -c -fPIC src/morse.c -o obj/morse_din.o
libcriptops.so: morse_din.o encriptacion_din.o
	gcc -shared -fPIC obj/encriptacion_din.o obj/morse_din.o -o lib/libcriptops.so
	export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/Desktop/taller_8/lib 
